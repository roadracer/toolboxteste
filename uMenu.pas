unit uMenu;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client, Inifiles, Vcl.ComCtrls;

type
  TfPrincipal = class(TForm)
    MainMenu1: TMainMenu;
    imArquivo: TMenuItem;
    imConfigurao: TMenuItem;
    este1: TMenuItem;
    imfProcurador: TMenuItem;
    imfSobre: TMenuItem;
    StatusBarPrincipal: TStatusBar;
    imfGeradorNumeroCNJ65: TMenuItem;
    imfAssessor: TMenuItem;
    procedure imConfiguraoClick(Sender: TObject);
    procedure imfProcuradorClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure imfSobreClick(Sender: TObject);
    procedure imfGeradorNumeroCNJ65Click(Sender: TObject);
    procedure imfAssessorClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    fConfig: TIniFile;

  end;

var
  fPrincipal: TfPrincipal;

implementation

uses
  uConfiguracaoBasedeDados, uProcurador, uSobre, uGeradorNumeroCNJ, uAssessor,
  uDatamodule;

{$R *.dfm}

procedure TfPrincipal.FormActivate(Sender: TObject);
begin
  fConfig := TIniFile.Create(ExtractFilePath(Application.ExeName) +
    'ToolBoxTesteBDcfg.ini');

  if fConfig.ReadString('BasedeDados', 'TipoBanco', 'Teste') = 'MSSQL' then
  begin
    dmDataModule.FDConnection.DriverName := 'MSSQL';
    dmDataModule.FDConnection.Params.Values['Database'] :=
      fConfig.ReadString('BasedeDados', 'Alias', 'InformeAqui');
    dmDataModule.FDConnection.Params.Values['Server'] :=
      fConfig.ReadString('BasedeDados', 'Server', 'InformeAqui');
  end
  else if fConfig.ReadString('BasedeDados', 'TipoBanco', '') = 'Oracle' then
  begin
    dmDataModule.FDConnection.DriverName := 'Ora';
    dmDataModule.FDConnection.Params.Values['DataBase'] :=
      fConfig.ReadString('BasedeDados', 'Alias', 'Teste');
  end;

  dmDataModule.FDConnection.Params.Values['User_name'] := 'saj';
  dmDataModule.FDConnection.Params.Values['Password'] := 'agesune1';

  dmDataModule.FDConnection.Open;

  if dmDataModule.FDConnection.Connected then
  begin
    StatusBarPrincipal.Panels[0].Text := 'Conex�o estabelecida';
    StatusBarPrincipal.Panels[1].Text := dmDataModule.FDConnection.Params.Values
      ['DataBase'];
  end
  else
  begin
    StatusBarPrincipal.Panels[0].Text := 'Conex�o n�o estabelecida';
  end;

end;

procedure TfPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfPrincipal.imConfiguraoClick(Sender: TObject);
var
  oform: TfConfiguracaoBasedeDados;
begin
  oform := TfConfiguracaoBasedeDados.Create(Self);
  oform.Show;
end;

procedure TfPrincipal.imfAssessorClick(Sender: TObject);
var
  oform: TfAssessor;
begin
  oform := TfAssessor.Create(Self);
  oform.Show;
end;

procedure TfPrincipal.imfGeradorNumeroCNJ65Click(Sender: TObject);
var
  oform: TfGeradorNumeroCNJ;
begin
  oform := TfGeradorNumeroCNJ.Create(Self);
  oform.Show;
end;

procedure TfPrincipal.imfProcuradorClick(Sender: TObject);
var
  oform: TfProcurador;
begin
  oform := TfProcurador.Create(Self);
  oform.Show;
end;

procedure TfPrincipal.imfSobreClick(Sender: TObject);
var
  oform: TfSobre;
begin
  oform := TfSobre.Create(Self);
  oform.Show;
end;

end.
