object fGeradorNumeroCNJ: TfGeradorNumeroCNJ
  Left = 0
  Top = 0
  Caption = 'Gerador de N'#250'mero CNJ'
  ClientHeight = 224
  ClientWidth = 310
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDesktopCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 17
    Top = 126
    Width = 81
    Height = 13
    Caption = 'D'#237'gito verificador'
  end
  object Label2: TLabel
    Left = 17
    Top = 145
    Width = 95
    Height = 13
    Caption = 'Ano de ajuizamento'
  end
  object Label3: TLabel
    Left = 17
    Top = 164
    Width = 30
    Height = 13
    Caption = #211'rg'#227'o'
  end
  object Label4: TLabel
    Left = 17
    Top = 183
    Width = 38
    Height = 13
    Caption = 'Tribunal'
  end
  object Label5: TLabel
    Left = 16
    Top = 202
    Width = 91
    Height = 13
    Caption = 'Unidade de Origem'
  end
  object Label6: TLabel
    Left = 17
    Top = 107
    Width = 90
    Height = 13
    Caption = 'N'#250'mero sequencial'
  end
  object Label7: TLabel
    Left = 8
    Top = 73
    Width = 90
    Height = 16
    Caption = 'N'#250'mero Judicial'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label8: TLabel
    Left = 16
    Top = 13
    Width = 49
    Height = 13
    Caption = 'NNNNNNN'
  end
  object Label9: TLabel
    Left = 71
    Top = 13
    Width = 14
    Height = 13
    Alignment = taCenter
    Caption = 'DD'
  end
  object Label10: TLabel
    Left = 128
    Top = 13
    Width = 28
    Height = 13
    Alignment = taCenter
    Caption = 'AAAA'
  end
  object Label11: TLabel
    Left = 187
    Top = 13
    Width = 5
    Height = 13
    Alignment = taCenter
    Caption = 'J'
  end
  object Label12: TLabel
    Left = 212
    Top = 13
    Width = 13
    Height = 13
    Alignment = taCenter
    Caption = 'TR'
  end
  object Label13: TLabel
    Left = 249
    Top = 13
    Width = 32
    Height = 13
    Alignment = taCenter
    Caption = 'OOOO'
  end
  object Gerar: TButton
    Left = 145
    Top = 121
    Width = 121
    Height = 26
    Caption = 'Gerar'
    TabOrder = 0
    OnClick = GerarClick
  end
  object Edit1: TEdit
    Left = 16
    Top = 32
    Width = 49
    Height = 21
    TabOrder = 1
  end
  object Edit2: TEdit
    Left = 71
    Top = 32
    Width = 25
    Height = 21
    TabOrder = 2
  end
  object Edit4: TEdit
    Left = 185
    Top = 32
    Width = 21
    Height = 21
    TabOrder = 3
  end
  object Edit5: TEdit
    Left = 212
    Top = 32
    Width = 23
    Height = 21
    TabOrder = 4
  end
  object Edit6: TEdit
    Left = 241
    Top = 32
    Width = 40
    Height = 21
    TabOrder = 5
  end
  object Edit7: TEdit
    Left = 104
    Top = 72
    Width = 194
    Height = 21
    TabOrder = 6
  end
  object ComboBox1: TComboBox
    Left = 102
    Top = 32
    Width = 77
    Height = 21
    TabOrder = 7
    Text = 'Ano'
    Items.Strings = (
      '1990'
      '1991'
      '1992'
      '1993'
      '1994'
      '1995'
      '1996'
      '1997'
      '1998'
      '1999'
      '2000'
      '2001'
      '2002'
      '2003'
      '2004'
      '2005'
      '2006'
      '2007'
      '2008'
      '2009'
      '2010'
      '2011'
      '2012'
      '2013'
      '2014'
      '2015'
      '2016'
      '2017'
      '2018'
      '2019'
      '2020'
      '2021'
      '2022'
      '2023'
      '2024'
      '2025'
      '2026'
      '2027'
      '2028'
      '2029'
      '2030'
      '2031'
      '2032'
      '2033'
      '2034'
      '2035'
      '2036'
      '2037'
      '2038'
      '2039'
      '2040')
  end
end
