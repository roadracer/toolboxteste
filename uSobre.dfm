object fSobre: TfSobre
  Left = 0
  Top = 0
  Caption = 'Sobre'
  ClientHeight = 136
  ClientWidth = 554
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDesktopCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object lbDesenvolvido: TLabel
    Left = 160
    Top = 24
    Width = 216
    Height = 19
    Caption = 'Desenvolvido por : Luiz Faria. '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 27
    Top = 64
    Width = 519
    Height = 19
    Caption = 
      'Apoio: Juliano Bartelli, Rafael Wagner, Robledo Fortuna, Valdeni' +
      'r Albino.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lbVersao: TLabel
    Left = 229
    Top = 102
    Width = 75
    Height = 16
    Caption = 'Vers'#227'o .: 1.0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
end
