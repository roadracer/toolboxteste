unit UConfiguracaoBasedeDados;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Inifiles,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  FireDAC.Stan.ExprFuncs, FireDAC.Phys.SQLiteDef, FireDAC.Phys.SQLite,
  Vcl.Buttons, Vcl.ComCtrls, FireDAC.VCLUI.Error, FireDAC.Phys.MSSQLDef,
  FireDAC.Phys.OracleDef, Vcl.ExtCtrls, FireDAC.Phys.Oracle,
  FireDAC.Phys.ODBCBase, FireDAC.Phys.MSSQL, FireDAC.Comp.UI;

type
  TfConfiguracaoBasedeDados = class(TForm)
    dfAliasBase: TEdit;
    dfServidor: TEdit;
    btSalvar: TSpeedButton;
    lbDiretorio: TLabel;
    Servidor: TLabel;
    StatusBar1: TStatusBar;
    rgTipodeBanco: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);

  private
    { Private declarations }
    fConfig: TIniFile;
    procedure ConectarBanco;
  public
    { Public declarations }
  end;

var
  fConfiguracaoBasedeDados: TfConfiguracaoBasedeDados;

implementation

{$R *.dfm}

uses uDatamodule, uMenu;

procedure TfConfiguracaoBasedeDados.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfConfiguracaoBasedeDados.FormCreate(Sender: TObject);
begin
  fConfig := TIniFile.Create(ExtractFilePath(Application.ExeName) +
    'ToolBoxTesteBDcfg.ini');

  if (fConfig.ReadString('BasedeDados', 'TipoBanco',
    rgTipodeBanco.Items[rgTipodeBanco.ItemIndex]) = EmptyStr) or
    (fConfig.ReadString('BasedeDados', 'Alias', dfAliasBase.Text) = EmptyStr)
  then
  Begin
    showmessage('Configure o arquivo ToolBoxTesteBDcfg.ini ');
    exit
  End
  Else
  Begin
    if fConfig.ReadString('BasedeDados', 'TipoBanco',
      rgTipodeBanco.Items[rgTipodeBanco.ItemIndex]) = 'MSSQL' then
      rgTipodeBanco.ItemIndex := 0
    Else
      rgTipodeBanco.ItemIndex := 1;

    dfAliasBase.Text := fConfig.ReadString('BasedeDados', 'Alias',
      dfAliasBase.Text);
    if rgTipodeBanco.Items[rgTipodeBanco.ItemIndex] = 'MSSQL' then
      dfServidor.Text := fConfig.ReadString('BasedeDados', 'Server',
        dfServidor.Text);
  End;

  ConectarBanco;
end;

procedure TfConfiguracaoBasedeDados.btSalvarClick(Sender: TObject);
begin

  fConfig := TIniFile.Create(ExtractFilePath(Application.ExeName) +
    'ToolBoxTesteBDcfg.ini');
  try
    fConfig.WriteString('BasedeDados', 'TipoBanco',
      rgTipodeBanco.Items[rgTipodeBanco.ItemIndex]);
    fConfig.WriteString('BasedeDados', 'Alias', dfAliasBase.Text);

    if { (fConfig.ValueExists('BasedeDados','Server')) or }
      (rgTipodeBanco.Items[rgTipodeBanco.ItemIndex] = 'MSSQL') then
      fConfig.WriteString('BasedeDados', 'Server', dfServidor.Text)
    Else
      exit;
  finally
    ConectarBanco;
    FreeAndNil(fConfig);
  end
end;

procedure TfConfiguracaoBasedeDados.ConectarBanco;
begin
  case rgTipodeBanco.ItemIndex of
    0:
      begin
        dmDataModule.FDConnection.DriverName := 'MSSQL';
        dmDataModule.FDConnection.Params.Values['Database'] :=
          fConfig.ReadString('BasedeDados', 'Alias', 'InformeAqui');
        dmDataModule.FDConnection.Params.Values['Server'] :=
          fConfig.ReadString('BasedeDados', 'Server', 'InformeAqui');
      end;
    1:
      Begin
        dmDataModule.FDConnection.DriverName := 'Ora';
        dmDataModule.FDConnection.Params.Values['DataBase'] :=
          fConfig.ReadString('BasedeDados', 'Alias', 'Teste');
      End
  else
    showmessage('Por favor, escolha um Tipo de Banco');
  end;

  dmDataModule.FDConnection.Params.Values['User_name'] := 'saj';
  dmDataModule.FDConnection.Params.Values['Password'] := 'agesune1';

  dmDataModule.FDConnection.Open;

  if dmDataModule.FDConnection.Connected then
  begin
    StatusBar1.Panels[0].Text := 'Conex�o estabelecida';
    StatusBar1.Panels[1].Text := dmDataModule.FDConnection.Params.Values
      ['DataBase'];
    fPrincipal.StatusBarPrincipal.Panels[0].Text := 'Conex�o estabelecida';
    fPrincipal.StatusBarPrincipal.Panels[1].Text :=
      dmDataModule.FDConnection.Params.Values['DataBase'];

  end
  else
  begin
    StatusBar1.Panels[0].Text := 'Conex�o n�o estabelecida';
  end;
end;

end.
