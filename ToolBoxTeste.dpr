program ToolBoxTeste;



uses
  Vcl.Forms,
  UConfiguracaoBasedeDados in 'UConfiguracaoBasedeDados.pas' {fConfiguracaoBasedeDados},
  Vcl.Themes,
  Vcl.Styles,
  uProcurador in 'uProcurador.pas' {fProcurador},
  uMenu in 'uMenu.pas' {fPrincipal},
  uDatamodule in 'uDatamodule.pas' {dmDataModule: TDataModule},
  uSobre in 'uSobre.pas' {fSobre},
  uGeradorNumeroCNJ in 'uGeradorNumeroCNJ.pas' {fGeradorNumeroCNJ},
  uAssessor in 'uAssessor.pas' {fAssessor};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfPrincipal, fPrincipal);
  Application.CreateForm(TdmDataModule, dmDataModule);
  TStyleManager.TrySetStyle('Tablet Light');
  Application.Run;
end.
