object fProcurador: TfProcurador
  Left = 0
  Top = 0
  Caption = 'Procurador'
  ClientHeight = 569
  ClientWidth = 707
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDesktopCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 15
    Width = 44
    Height = 19
    Caption = 'Chefia'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 79
    Width = 120
    Height = 19
    Caption = 'Vardist da Chefia'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 8
    Top = 207
    Width = 181
    Height = 19
    Caption = 'Procurador(es) da Vardist'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 472
    Top = 156
    Width = 185
    Height = 19
    Caption = 'Situa'#231#227'o do Procurador(a)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object grDadosProcurador: TDBGrid
    Left = 8
    Top = 232
    Width = 681
    Height = 287
    DataSource = dsDadosProcurador
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'NMPESSOA'
        Width = 300
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CDUSUARIO'
        Width = 160
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CDVARDIST'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CDSITUACAOPROCUR'
        Width = 120
        Visible = True
      end>
  end
  object pbConsultar: TButton
    Left = 8
    Top = 525
    Width = 75
    Height = 25
    Caption = 'Consultar'
    TabOrder = 3
    OnClick = pbConsultarClick
  end
  object DBLookupCBChefia: TDBLookupComboBox
    Left = 8
    Top = 40
    Width = 681
    Height = 21
    KeyField = 'CDCHEFIA'
    ListField = 'CC_CHEFIA'
    ListSource = dsChefia
    TabOrder = 0
  end
  object DBLookupCBVardist: TDBLookupComboBox
    Left = 8
    Top = 104
    Width = 681
    Height = 21
    KeyField = 'CDVARDIST'
    ListField = 'CC_VARDIST'
    ListSource = dsVardist
    TabOrder = 1
  end
  object DBLookupCBSituacaoprocur: TDBLookupComboBox
    Left = 472
    Top = 181
    Width = 217
    Height = 21
    KeyField = 'CDSITUACAOPROCUR'
    ListField = 'DESITUACAOPROCUR'
    ListSource = dsDadosSituacaoprocur
    TabOrder = 2
  end
  object FDQueryChefia: TFDQuery
    Connection = dmDataModule.FDConnection
    SQL.Strings = (
      
        'select c.cdchefia, c.dechefia,{if Ora} c.cdchefia || '#39' - '#39' || c.' +
        'dechefia {fi} {if MSSQL} (cast(c.cdchefia as Varchar) + '#39' - '#39' + ' +
        'c.dechefia) {fi}  as cc_chefia from espjchefia c where c.flforau' +
        'so = '#39'N'#39' order by c.cdchefia')
    Left = 400
    Top = 32
  end
  object FDQueryDadosProcurador: TFDQuery
    Connection = dmDataModule.FDConnection
    SQL.Strings = (
      
        'select distinct e.cdvardist,  v.NMPESSOA, u.cdusuario, h.cdsitua' +
        'caoprocur'
      'from espjvardistprocur e,'
      '     espjvarprocurhist h,'
      '     vspjprocurador v,'
      '     espjusuario u'
      'where v.CDPESSOA=u.cdpessoa and'
      '      e.cdprocurador = v.CDPESSOA and'
      '      e.cdprocurador = h.cdprocurador and'
      '      e.cdvardist = h.cdvardist and                    '
      '      h.cdsituacaoprocur =:cdsituacaoprocur and'
      '      e.cdvardist =:cdvardist and'
      
        '      h.dtinicio <= {if Ora} sysdate {fi} {if MSSQL} getdate() {' +
        'fi} and'
      
        '      (h.dtfim is null or h.dtfim >= {if Ora} sysdate {fi} {if M' +
        'SSQL} getdate() {fi})')
    Left = 192
    Top = 352
    ParamData = <
      item
        Name = 'CDSITUACAOPROCUR'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'CDVARDIST'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object dsDadosProcurador: TDataSource
    AutoEdit = False
    DataSet = FDQueryDadosProcurador
    Left = 320
    Top = 352
  end
  object FDQueryVardist: TFDQuery
    MasterSource = dsChefia
    Connection = dmDataModule.FDConnection
    SQL.Strings = (
      
        'select cdvardist, cdchefia, {if Ora} cdvardist || '#39' -> '#39' ||  cdc' +
        'hefia || '#39' - '#39' || dechefia || '#39' - '#39' || cdarea || '#39' - '#39' || dearea' +
        ' || '#39' - '#39' || cdprocuradoria || '#39' - '#39' || delocal {fi} {if MSSQL} ' +
        '(cast(cdvardist as varchar) + '#39' -> '#39' + cast(cdchefia as varchar)' +
        ' + '#39' - '#39' + dechefia + '#39' - '#39' + cast(cdarea as varchar) + '#39' - '#39' + ' +
        'dearea + '#39' - '#39' + cast(cdprocuradoria as varchar) + '#39' - '#39' + deloc' +
        'al) {fi} as cc_vardist from v_vardist where cdchefia = :cdchefia'
      '')
    Left = 400
    Top = 96
    ParamData = <
      item
        Name = 'CDCHEFIA'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object dsChefia: TDataSource
    DataSet = FDQueryChefia
    Left = 432
    Top = 32
  end
  object dsVardist: TDataSource
    DataSet = FDQueryVardist
    Left = 440
    Top = 96
  end
  object FDQuerySituacaoprocur: TFDQuery
    Connection = dmDataModule.FDConnection
    SQL.Strings = (
      
        'select cdsituacaoprocur, desituacaoprocur from espjsituacaoprocu' +
        'r where flforauso = '#39'N'#39)
    Left = 568
    Top = 176
  end
  object dsDadosSituacaoprocur: TDataSource
    DataSet = FDQuerySituacaoprocur
    Left = 624
    Top = 176
  end
end
