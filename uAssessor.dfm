object fAssessor: TfAssessor
  Left = 0
  Top = 0
  Caption = 'Assessor'
  ClientHeight = 637
  ClientWidth = 549
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDesktopCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = -5
    Top = 0
    Width = 546
    Height = 321
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 160
      Height = 19
      Caption = 'Assessor(es) da Chefia'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object dbAssessorChefia: TDBGrid
      Left = 16
      Top = 88
      Width = 320
      Height = 185
      DataSource = dsDadosAssessorChefia
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
    end
    object DBLookupComboBoxChefiaAssessor: TDBLookupComboBox
      Left = 16
      Top = 49
      Width = 320
      Height = 21
      KeyField = 'CDCHEFIA'
      ListField = 'CC_CHEFIA'
      ListSource = dsChefiaAssessor
      TabOrder = 1
    end
    object Button2: TButton
      Left = 16
      Top = 279
      Width = 75
      Height = 25
      Caption = 'Consultar'
      TabOrder = 2
      OnClick = Button2Click
    end
  end
  object TPanel
    Left = -5
    Top = 327
    Width = 546
    Height = 314
    TabOrder = 1
    object TLabel
      Left = 16
      Top = 16
      Width = 172
      Height = 19
      Caption = 'Assessor(es) Procurador'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 144
      Top = 280
      Width = 128
      Height = 16
      Caption = 'Usu'#225'rio do Procurador'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object dbDadosAssessorProcurador: TDBGrid
      Left = 16
      Top = 41
      Width = 320
      Height = 224
      DataSource = dsDadosAssessorProcurador
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
    end
    object Button1: TButton
      Left = 16
      Top = 280
      Width = 75
      Height = 25
      Caption = 'Consultar'
      TabOrder = 1
      OnClick = Button1Click
    end
    object dfUsuarioProcurador: TEdit
      Left = 286
      Top = 279
      Width = 121
      Height = 21
      TabOrder = 2
    end
  end
  object FDQueryAssessorChefia: TFDQuery
    MasterSource = dsChefiaAssessor
    MasterFields = 'CDCHEFIA'
    Connection = dmDataModule.FDConnection
    SQL.Strings = (
      'select cdchefia, cdusuario'
      '  from espjvincchefiausu'
      ' where cdchefia =:cdchefia')
    Left = 72
    Top = 120
    ParamData = <
      item
        Name = 'CDCHEFIA'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object dsDadosAssessorChefia: TDataSource
    DataSet = FDQueryAssessorChefia
    Left = 232
    Top = 120
  end
  object FDQueryAssessorProcurador: TFDQuery
    Connection = dmDataModule.FDConnection
    Left = 80
    Top = 399
  end
  object dsDadosAssessorProcurador: TDataSource
    DataSet = FDQueryAssessorProcurador
    Left = 224
    Top = 399
  end
  object FDQueryChefiaAssessor: TFDQuery
    Connection = dmDataModule.FDConnection
    SQL.Strings = (
      
        'select c.cdchefia, c.dechefia,{if Ora} c.cdchefia || '#39' - '#39' || c.' +
        'dechefia {fi} {if MSSQL} (cast(c.cdchefia as Varchar) + '#39' - '#39' + ' +
        'c.dechefia) {fi}  as cc_chefia from espjchefia c where c.flforau' +
        'so = '#39'N'#39' order by c.cdchefia')
    Left = 299
    Top = 8
  end
  object dsChefiaAssessor: TDataSource
    DataSet = FDQueryChefiaAssessor
    Left = 435
    Top = 8
  end
end
