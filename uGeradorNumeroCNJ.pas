unit uGeradorNumeroCNJ;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Math, Vcl.StdCtrls, Vcl.ToolWin,
  Vcl.ComCtrls, DateUtils;

type
  TfGeradorNumeroCNJ = class(TForm)
    Gerar: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit4: TEdit;
    Edit5: TEdit;
    Edit6: TEdit;
    Edit7: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    ComboBox1: TComboBox;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    procedure GerarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    function MontaNumeroCNJ(NNNNNNN, DD, AAAA, J, TR, OOOO: string): string;
  function CalculaDigitoVerificadorCNJ(NNNNNNN, AAAA,
  JTR, OOOO: string): string;
  function PreencheZeros(psString: string; pTamanho: integer): string;
  procedure PreencherNumerosAleatorios;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fGeradorNumeroCNJ: TfGeradorNumeroCNJ;

implementation

{$R *.dfm}

{ TForm1 }


procedure TfGeradorNumeroCNJ.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfGeradorNumeroCNJ.FormCreate(Sender: TObject);
begin
  ComboBox1.ItemIndex := ComboBox1.items.IndexOf(IntToStr(yearof(now)));
end;

procedure TfGeradorNumeroCNJ.GerarClick(Sender: TObject);
begin
  PreencherNumerosAleatorios;
  //ComboBox1.Items[ComboBox1.ItemIndex] := IntToStr(yearof(now));
  MontaNumeroCNJ(Edit1.Text, Edit2.Text, {Edit3.Text} ComboBox1.Items[ComboBox1.ItemIndex], Edit4.Text, Edit5.Text, Edit6.Text);
end;

function TfGeradorNumeroCNJ.CalculaDigitoVerificadorCNJ(NNNNNNN, AAAA, JTR,
  OOOO: string): string;
var
  sValor1, sValor2, sValor3, deDigitoVerificador: string;
  resto1, resto2, resto3, digitoVerificador: longint;
begin
  sValor1 := PreencheZeros(NNNNNNN, 7);
  resto1 := StrToInt(sValor1) mod 97;
  sValor2 := PreencheZeros(IntToStr(resto1), 2) + PreencheZeros(AAAA, 4) +
    PreencheZeros(JTR, 3);
  resto2 := StrToInt(sValor2) mod 97;
  sValor3 := PreencheZeros(IntToStr(resto2), 2) + PreencheZeros(OOOO, 4) + '00';
  resto3 := StrToInt(sValor3) mod 97;
  digitoVerificador := 98 - resto3;
  deDigitoVerificador := PreencheZeros(IntToStr(digitoVerificador), 2);
  result := deDigitoVerificador;
end;

function TfGeradorNumeroCNJ.MontaNumeroCNJ(NNNNNNN, DD, AAAA, J, TR, OOOO: string): string;
var
  nuFormatadoNovo: string;
begin
  //0003265-93.2005.9.26.0020
  //if pbCalculaDigitoVeridicador then
    DD := CalculaDigitoVerificadorCNJ(NNNNNNN, AAAA, J + TR, OOOO);

  nuFormatadoNovo := PreencheZeros(NNNNNNN, 7) + '-' + PreencheZeros(DD, 2);
  nuFormatadoNovo := nuFormatadoNovo + '.' + PreencheZeros(AAAA, 4);
  nuFormatadoNovo := nuFormatadoNovo + '.' + J[1];
  nuFormatadoNovo := nuFormatadoNovo + '.' + PreencheZeros(TR, 2);
  nuFormatadoNovo := nuFormatadoNovo + '.' + PreencheZeros(OOOO, 4);
  Result := nuFormatadoNovo;
  Edit7.Text := nuFormatadoNovo;
end;



procedure TfGeradorNumeroCNJ.PreencherNumerosAleatorios;
begin
  Edit1.Text := IntToStr(Random(9999999));
  Edit2.Text := IntToStr(Random(99));
  //Edit3.Text := IntToStr(RandomRange(1900,2010));
  Edit4.Text := IntToStr(RandomRange(1,9));
  Edit5.Text := IntToStr(RandomRange(0,90));
  Edit6.Text := IntToStr(RandomRange(0001,8999));
end;

function TfGeradorNumeroCNJ.PreencheZeros(psString: string; pTamanho: integer): string;
var
  i, len: integer;
begin
  len := length(psString);
  for i := len + 1 to pTamanho do
    psString := '0' + psString;
  result := psString;
end;

end.
