unit uAssessor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB, Vcl.Grids,
  Vcl.DBGrids, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.StdCtrls,
  Vcl.ExtCtrls, Vcl.DBCtrls;

type
  TfAssessor = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    FDQueryAssessorChefia: TFDQuery;
    dsDadosAssessorChefia: TDataSource;
    dbAssessorChefia: TDBGrid;
    FDQueryAssessorProcurador: TFDQuery;
    dsDadosAssessorProcurador: TDataSource;
    dbDadosAssessorProcurador: TDBGrid;
    Button1: TButton;
    dfUsuarioProcurador: TEdit;
    Label2: TLabel;
    DBLookupComboBoxChefiaAssessor: TDBLookupComboBox;
    FDQueryChefiaAssessor: TFDQuery;
    dsChefiaAssessor: TDataSource;
    Button2: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fAssessor: TfAssessor;

implementation

{$R *.dfm}

uses uDatamodule;

procedure TfAssessor.Button1Click(Sender: TObject);
begin
  if not FDQueryAssessorProcurador.Active then
  begin
    FDQueryAssessorProcurador.SQL.Add
      ('select pr.cdusuario as procurador, ul.cdusuario as assessor');
    FDQueryAssessorProcurador.SQL.Add
      ('from esajusuariolotacao ul join espjusuario pr');
    FDQueryAssessorProcurador.SQL.Add('on pr.cdpessoa = ul.cdlocal');
    FDQueryAssessorProcurador.SQL.Add('where ul.cdtipolocal = 2');
    FDQueryAssessorProcurador.SQL.Add('and pr.cdusuario <> ul.cdusuario');
    FDQueryAssessorProcurador.SQL.Add('order by 1, 2');
    FDQueryAssessorProcurador.Open
  end;

  // FDQueryAssessorProcurador.Open;
  FDQueryAssessorProcurador.Filtered := False;
  if trim(dfUsuarioProcurador.text) <> '' then
  begin
    FDQueryAssessorProcurador.Filter := 'procurador like upper(''%' +
      dfUsuarioProcurador.text + '%'')';
    FDQueryAssessorProcurador.Filtered := True;
  end;
end;

procedure TfAssessor.Button2Click(Sender: TObject);
begin
  FDQueryAssessorChefia.Open;
  if FDQueryAssessorChefia.IsEmpty then
  begin
    ShowMessage('N�o existe Assessor da Chefia!')
  end

end;

procedure TfAssessor.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfAssessor.FormShow(Sender: TObject);
begin
  if dmDataModule.FDConnection.Connected then
    FDQueryChefiaAssessor.Open;
end;

end.
