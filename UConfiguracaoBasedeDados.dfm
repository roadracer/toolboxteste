object fConfiguracaoBasedeDados: TfConfiguracaoBasedeDados
  Left = 0
  Top = 0
  Caption = 'Configura'#231#227'o da base de dados'
  ClientHeight = 186
  ClientWidth = 411
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDesktopCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object btSalvar: TSpeedButton
    Left = 21
    Top = 120
    Width = 76
    Height = 22
    Caption = 'Conectar'
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000130B0000130B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333330070
      7700333333337777777733333333008088003333333377F73377333333330088
      88003333333377FFFF7733333333000000003FFFFFFF77777777000000000000
      000077777777777777770FFFFFFF0FFFFFF07F3333337F3333370FFFFFFF0FFF
      FFF07F3FF3FF7FFFFFF70F00F0080CCC9CC07F773773777777770FFFFFFFF039
      99337F3FFFF3F7F777F30F0000F0F09999937F7777373777777F0FFFFFFFF999
      99997F3FF3FFF77777770F00F000003999337F773777773777F30FFFF0FF0339
      99337F3FF7F3733777F30F08F0F0337999337F7737F73F7777330FFFF0039999
      93337FFFF7737777733300000033333333337777773333333333}
    NumGlyphs = 2
    OnClick = btSalvarClick
  end
  object lbDiretorio: TLabel
    Left = 21
    Top = 26
    Width = 52
    Height = 14
    Caption = 'Alias Base'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Servidor: TLabel
    Left = 21
    Top = 66
    Width = 44
    Height = 14
    Caption = 'Servidor'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object dfAliasBase: TEdit
    Left = 96
    Top = 24
    Width = 145
    Height = 21
    TabOrder = 0
  end
  object dfServidor: TEdit
    Left = 96
    Top = 64
    Width = 145
    Height = 21
    TabOrder = 1
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 168
    Width = 411
    Height = 18
    Panels = <
      item
        Width = 180
      end
      item
        Width = 50
      end>
  end
  object rgTipodeBanco: TRadioGroup
    Left = 264
    Top = 8
    Width = 105
    Height = 105
    Caption = 'Tipo de Banco'
    ItemIndex = 0
    Items.Strings = (
      'MSSQL'
      'Oracle')
    TabOrder = 3
  end
end
