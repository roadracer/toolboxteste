unit uProcurador;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.Oracle, FireDAC.Phys.OracleDef, FireDAC.VCLUI.Wait,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Grids, Vcl.DBGrids,
  Datasnap.Provider, Vcl.DBCtrls, System.UITypes;

type
  TfProcurador = class(TForm)
    FDQueryChefia: TFDQuery;
    grDadosProcurador: TDBGrid;
    FDQueryDadosProcurador: TFDQuery;
    pbConsultar: TButton;
    dsDadosProcurador: TDataSource;
    FDQueryVardist: TFDQuery;
    dsChefia: TDataSource;
    dsVardist: TDataSource;
    DBLookupCBChefia: TDBLookupComboBox;
    DBLookupCBVardist: TDBLookupComboBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBLookupCBSituacaoprocur: TDBLookupComboBox;
    FDQuerySituacaoprocur: TFDQuery;
    dsDadosSituacaoprocur: TDataSource;
    Label4: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure pbConsultarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fProcurador: TfProcurador;

implementation

{$R *.dfm}

uses UConfiguracaoBasedeDados, uDatamodule;

procedure TfProcurador.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfProcurador.FormShow(Sender: TObject);
begin
  if dmDataModule.FDConnection.Connected then
  Begin
    FDQueryChefia.Open;
    FDQueryVardist.Open;
    FDQuerySituacaoprocur.Open;
    // FDQuerySituacaoprocur.SQL.Add('select desituacaoprocur from espjsituacaoprocur where flforauso = ''N''');
    // FDQuerySituacaoprocur.SQL.add('select cdsituacaoprocur from espjsituacaoprocur where cdsituacaoprocur =:Pcdsituacaoprocur');
    // FDQuerySituacaoprocur.ParamByName('cdsituacaoprocur').AsString := DBLookupCBSituacaoprocur.Text;
  End
  Else
    case MessageDlg
      ('Realize a conex�o com o Banco de Dados em Arquivo, Configura��es.',
      mtWarning, [mbOK, mbCancel], 0) of
      mrOk:
        begin
          // Write code here for pressing button OK
          // ShowMessage('Realize a conex�o com o Banco de Dados em Arquivo, Configura��es.');
          close;
        end;
      mrCancel:
        begin
          // Write code here for pressing button Cancel
          ShowMessage('Apertou o bot�o Cancelar.');
        end;
      // ShowMessage('Realize a conex�o com o Banco de Dados em Arquivo, Configura��es');

    end;
end;

procedure TfProcurador.pbConsultarClick(Sender: TObject);
begin
  FDQueryDadosProcurador.close;
  FDQueryDadosProcurador.ParamByName('cdsituacaoprocur').AsString :=
    DBLookupCBSituacaoprocur.KeyValue;
  FDQueryDadosProcurador.ParamByName('cdvardist').AsString :=
    DBLookupCBVardist.KeyValue;
  FDQueryDadosProcurador.Open;
  if FDQueryDadosProcurador.IsEmpty then
  begin
    showmessage('N�o existe Procurador: ' + DBLookupCBSituacaoprocur.Text)
  end

end;

end.
